/*
Задание 1
Разработайте страницу "текстовый редактор". На странице должно быть размещенно поле ввода и кнопка "Сохранить" (фактического сохранения делать не надо). Если пользователь пытается закрыть окно браузера не "сохранив" данные в поле ввода, должно запускаться окно, которое потребует подтверждения операции закрытия окна.

Задание 2
Создайте страницу с несколькими блоками текста. Реализуйте обработчики событий таким образом, чтобы при нажатии на кнопку r - текст становился красного цвета, кнопка g сделает цвет текста зеленым, а b синим. 

Задание 3
Сделайте кнопку с надписью "получить скидку". При наведении кнопка должна "убегать" от курсора е давая пользователю нажать себя.

Задание 4 
Разработайте страницу, которая будет выводить сообщение "сохранено"при нажатии на клавиши Ctrl + S, "выбранно все" при нажатии на Ctrl+A и "сохранено все" при нажатии на комбинацию Ctrl+Shift+S.
*/ 

// 1 задание
const textField = document.querySelector('#text-field');
const btnSave = document.querySelector('#sbmt');
let changed = false;

textField.onchange = function() {
    changed = true;
}

btnSave.addEventListener('click', (e) => {
    e.preventDefault();
    changed = false;
});

window.addEventListener('beforeunload', e => {
    if(changed) {
        e.preventDefault();
        e.returnValue = '?';
    }
})

// 2 задание

const lorem = document.querySelector('#lorem');

document.addEventListener('keydown', function(e) {
    switch(e.code) {
        case 'KeyR':
            lorem.style.color = 'red';
            break;
        case 'KeyG':
            lorem.style.color = 'green';
            break;
        case 'KeyB':
            lorem.style.color = 'blue';
            break;
        default:
            lorem.style.color = 'black';
    }
})

// 3 задание

const btn = document.querySelector('#btn');

btn.addEventListener('mouseenter', () => {
    btn.style.left = `${random(550)}px`;
    btn.style.top = `${random(250)}px`;
})

function random(x) {
    const y = Math.floor( Math.random() * x);
    return y;
}


// 4 задание

document.addEventListener('keydown', (e) => {
    console.log(e.code);
    if (e.ctrlKey && e.shiftKey && e.code == 'KeyS'){
        e.preventDefault();
        alert('сохранено все');
    } else if (e.ctrlKey && e.code == 'KeyA') {
        e.preventDefault();
        alert('выбранно все');
    } else if (e.ctrlKey  && e.code == 'KeyS') {
        e.preventDefault();
        alert('сохранено');
    }
});